require 'spec_helper'

RSpec.describe Video, type: :model do
  describe 'creating a valid video' do
    let(:valid_video) { FactoryGirl.create :video, extension: 'mp4' }

    it { expect(valid_video).to be_valid }
    it { expect(valid_video).to be_pending }
    it { expect(valid_video).to have_attributes(name: 'test_video.mp4',
      name_without_extension: 'test_video') }

    it 'should create a file on local storage (dev environment)' do
      assert_match "#{@bucket}#{valid_video.id}/#{valid_video.original_file_identifier}",
       valid_video.original_file.url
    end

    it 'change the state to transcoding on scheduled' do
      valid_video.scheduled!
      expect(valid_video).to be_transcoding
    end

    it 'change the state to transcoded on finished' do
      valid_video.scheduled!
      valid_video.finished!
      expect(valid_video).to be_transcoded
    end
  end

  describe 'creating an invalid video' do
    let(:invalid_video) { FactoryGirl.build :video, extension: 'txt' }
    it 'prevent create videos with invalid extension' do
      expect(invalid_video).not_to be_valid
    end
  end
end
