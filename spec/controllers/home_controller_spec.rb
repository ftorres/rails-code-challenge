require 'spec_helper'

RSpec.describe HomeController, type: :controller do
  render_views

  describe "GET #index" do
    it "must reach index action" do
      get :index
      assert_response :success
      assert_template 'home/index'
    end
  end
end
