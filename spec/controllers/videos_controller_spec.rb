require 'spec_helper'

RSpec.describe VideosController, type: :controller do
  render_views

  describe 'GET #index' do
    it 'must render video index page' do
      get :index
      assert_response :success
      assert_template 'videos/index'
    end

    it 'when there is no video, should render empty list' do
      get :index
      assert_template partial: '_empty_list'
    end

    it 'when there is videos, should render list' do
      3.times do
        FactoryGirl.create :video, extension: 'mp4'
      end
      get :index
      assert_template partial: '_video', count: 3
      assert_equal assigns(:videos), Video.all
    end
  end

  describe 'GET #new' do
    it 'should load upload form' do
      get :new
      expect(response).to render_template(partial: '_form')
      assert_equal assigns(:video).class, Video.new.class
    end
  end

  describe 'POST #create' do
    include ActiveJob::TestHelper

    it 'should upload a new video and schedule a job to ZencoderJob' do
     post :create, params: { video:
        FactoryGirl.attributes_for(:video, :extension => 'mp4') }
     expect(enqueued_jobs.size).to eq(1)
     expect(response).to redirect_to videos_url
    end

    it 'should prevent upload blank files' do
      post :create, params: { video: {} }
      expect(response.body).to match /Original file can.*t be blank/im
    end

    it 'should prevent upload invalid files' do
      post :create, params: { video:
         FactoryGirl.attributes_for(:video, :extension => 'txt') }
      expect(response.body).to match /You are not allowed to upload .* files/im
    end
  end

  describe 'GET #show' do
    it 'should present a 404 error if video not found' do
      get :show, params: { id: 1000 }
      expect(response.status).to eq(404)
      expect(response.body).to match /This video no longer exists/im
    end

    it 'should present the apropriated partial for te selected video' do
      video = FactoryGirl.create :video, :extension => 'mp4'
      video.scheduled!

      get :show, params: { id: video.id }
      expect(response).to render_template(partial: '_player_transcoding')

      video.finished!
      get :show, params: { id: video.id }
      expect(response).to render_template(partial: '_player_transcoded')
    end
  end
end
