FactoryGirl.define do
  factory :video do
    transient do
      extension 'mp4'
    end

    original_file { Rack::Test::UploadedFile.new(File.open(File.join(Rails.root,
     "/spec/support/fixtures/files/test_video.#{extension}"))) }
  end
end
