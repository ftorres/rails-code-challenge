class Video < ApplicationRecord
  default_scope { order("created_at DESC") }

  scope :with_job, -> { where('job_id is not null') }
  scope :pending, -> { with_state(:pending) }
  scope :transcoding, -> { with_job.with_state(:transcoding) }
  scope :transcoded, -> { with_job.with_state(:transcoded) }
  scope :failed, -> { with_job.with_state(:failed) }

  mount_uploader :original_file, VideoUploader

  validates_presence_of :original_file

  state_machine initial: :pending do
    event :scheduled do
      transition :pending => :transcoding
    end

    event :finished do
      transition :transcoding => :transcoded
    end

    event :failed do
      transition [:pending, :transcoding] => :failed
    end
  end

  def name
    original_file_identifier
  end

  def name_without_extension
    name.split('.')[0...-1].join
  end

  def readable_date
    created_at.strftime('%d-%b %H:%M')
  end

  def readable_size
    if size_in_bytes
      "#{size_in_bytes / 1024} Kbytes"
    else
      "Unavailable"
    end
  end

  def readable_duration
    if duration
      "#{duration / 1000} secs"
    else
      "Unavailable"
    end
  end

  def has_errors?
    errors.present?
  end
end
