# Utitlity class to create a hash representing a new
#  job request to send to Zencoder api
class ZencoderJobBuilder
  def initialize(video)
    @video = video
  end

  def build_payload
    {
      input: "s3://videoencodercodechallenge/#{Rails.env}/originals/#{@video.id}/#{@video.name}",
      outputs: [
        {
          label: "#{@video.name} (MP4)",
          url: "s3://videoencodercodechallenge/#{Rails.env}/transcoded/#{@video.id}/#{@video.name_without_extension}.mp4",
          h264_profile: 'high',
          public: true
        }
      ]
    }
  end
end
