module ApplicationHelper
  # Helper classes to build the main menu marking the
  #  active menu item with different css class
  module UI
    # Represents a item in a menu. The tag attribute is used
    #  to decorate each item with a TAG css class
    class MenuItem
      attr_accessor :url, :tag, :active, :title

      def initialize(params)
        @url = params[:url]
        @tag = params[:tag]
        @active = params[:active]
        @title = params[:title]
      end

      def css_class
        classes = ['mdl-layout__tab']
        classes << 'is-active' if active
        classes.join(' ')
      end
    end

    # Used to build the application's menu on each page
    class Menu
      delegate :url_helpers, to: 'Rails.application.routes'

      def initialize(active_item = :home)
        @active_item = active_item
      end

      def items
        build_items.each do |item|
          item.active = active_item?(item.tag)
        end
      end

      private

      def active_item?(tag)
        @active_item == tag
      end

      def build_items
        [
          MenuItem.new(title: 'Overview', tag: :home,
                       url: url_helpers.root_path,
                       active: true),

          MenuItem.new(title: 'Videos', tag: :videos,
                       url: url_helpers.videos_path,
                       active: false)
        ]
      end
    end

    # Display flash messages
    class Flash
      attr_accessor :msg

      def initialize(msg = nil)
        @msg = msg
      end

      def show
        return if msg.nil?
        "<div class='alert-info'>#{msg}</div>".html_safe
      end
    end
  end
end
