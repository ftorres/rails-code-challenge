# Job to make polling on Zencoder and query for
#   job status
require 'sidekiq-scheduler'

class ZencoderPollingWorker
  include Sidekiq::Worker

  def perform
    transcoding = Video.transcoding
    puts "Quering job status for #{transcoding.size} Jobs" if transcoding.any?

    transcoding.each do |video|
      puts "Job id #{video.job_id}"
      response = Zencoder::Job.details(video.job_id)
      if response.code.to_i == 200
        body = response.body
        job = body['job']
        state = job['state']

        case state
        when "finished" then
          output = job['output_media_files'][0]
          video.finished
          video.converted_file = output['url']
          video.duration = output['duration_in_ms']
          video.size_in_bytes = output['file_size_bytes']
          video.width = output['width']
          video.height = output['height']
          video.save
        when "failed"
          video.failed!
        end
      end
    end
  end
end
