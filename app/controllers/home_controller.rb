class HomeController < ApplicationController
  # Home controller displays only the welcome screen
  #  with information about the application
  def index; end
end
