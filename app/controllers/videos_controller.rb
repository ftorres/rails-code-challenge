class VideosController < ApplicationController
  def index
    @videos = Video.all
  end

  def new
    @video = Video.new
  end

  def create
    @video = Video.new(parameters)
    video_scheduler = VideoSchedulerService.new(@video)

    if video_scheduler.create
      redirect_to videos_path, notice: 'Your video has been scheduled to encode'
    else
      render action: :new
    end
  end

  def show
    @video = Video.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render file: 'public/404.html', status: :not_found
  end

  private

  def parameters
    params.fetch(:video, {}).permit(:original_file)
  end
end
