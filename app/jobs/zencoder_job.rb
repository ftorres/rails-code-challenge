# Job to provide asynchronous integration with
#   Zencoder api. After a new video creation it post a request
#   to convert the uploaded file to another format

class ZencoderJob < ActiveJob::Base
  queue_as :default

  def perform(video_id)
    video = Video.find(video_id)
    job = ZencoderJobBuilder.new(video)

    begin
      response = Zencoder::Job.create(job.build_payload, :timeout => 30_000)
      if response.code.to_i == 201
        video.job_id = response.body['id']
        video.scheduled
        video.save
      end
    rescue Timeout::Error, Zencoder::HTTPError
      # Wait 10 seconds to reschedule the job in case of failure
      ZencoderJob.set(wait: 10.seconds).perform_later(video_id)
    end
  end
end
