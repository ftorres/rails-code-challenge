[![pipeline status](https://gitlab.com/ftorres/rails-code-challenge/badges/master/pipeline.svg)](https://gitlab.com/ftorres/rails-code-challenge/commits/master)
[![coverage report](https://gitlab.com/ftorres/rails-code-challenge/badges/master/coverage.svg)](https://gitlab.com/ftorres/rails-code-challenge/commits/master)

# Ruby on Rails code challenge

The challenge consists in develop a Web application that accepts a video upload
in some valid video formats for web and transcode it to MP4.

## Requirements:

- Upload files to an AWS S3 bucket
- Integrate the uploaded files with Zencoder API in order to transcode to MP4
- Use a JS player to allow the user to watch the transcoded video
- Use Ruby on Rails as development platform
- Implement unit and integration tests

## Overview

![overview image](code_challenge_overview.png "Application Overview")

## Screen play

Click on the image down below to watch a video with it in action

[![Watch the video](https://s3.amazonaws.com/videoencodercodechallenge/screen_play_codechallenge.png)](https://s3.amazonaws.com/videoencodercodechallenge/screen_play_codechallenge.mp4)
