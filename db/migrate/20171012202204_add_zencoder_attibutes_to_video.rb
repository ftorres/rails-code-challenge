class AddZencoderAttibutesToVideo < ActiveRecord::Migration[5.1]
  def change
    change_table(:videos) do |t|
      t.integer :job_id
      t.string :converted_file
      t.integer :width
      t.integer :height
      t.integer :size_in_bytes
      t.integer :duration
    end
  end

  def down
    remove_column :videos, :job_id
    remove_column :videos, :converted_file
    remove_column :videos, :width
    remove_column :videos, :height
    remove_column :videos, :size_in_bytes
    remove_column :videos, :duration
  end
end
