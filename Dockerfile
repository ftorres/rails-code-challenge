FROM ruby:2.4

RUN apt-get update && apt-get install -y ruby-dev nodejs
RUN mkdir /app
WORKDIR /app

COPY Gemfile Gemfile.lock ./
RUN bundle install --binstubs

COPY . .

CMD /app/boot.sh
