#!/bin/bash

RAILS_ENV=production bundle exec rails db:migrate && \
  bundle exec rails assets:precompile && \
  bundle exec sidekiq -d -L log/sidekiq.log && \
  puma -C config/puma.rb
